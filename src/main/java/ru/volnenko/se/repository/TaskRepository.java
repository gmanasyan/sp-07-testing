package ru.volnenko.se.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.volnenko.se.entity.Project;
import ru.volnenko.se.entity.Task;

@Repository
public interface TaskRepository extends JpaRepository<Task, String> {

    Task findByName(String name);

}
