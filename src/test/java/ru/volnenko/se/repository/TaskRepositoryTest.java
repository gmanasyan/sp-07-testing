package ru.volnenko.se.repository;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;
import ru.volnenko.se.entity.Project;
import ru.volnenko.se.entity.Task;

/**
 *  Spring Jpa test with no spring container
 *  - using in memory Dd with rollback after each test
 *  - autowired beans by spring
 */

@DataJpaTest
@RunWith(SpringRunner.class)
public class TaskRepositoryTest {

    @Autowired
    private ProjectRepository projectRepository;

    @Autowired
    private TaskRepository taskRepository;

    @Test
    public void addTaskTest() {
        Assert.assertNull(taskRepository.findByName("Test"));
        final Task task = new Task();
        task.setName("Test");
        taskRepository.save(task);
        Assert.assertNotNull(taskRepository.findByName("Test"));
    }

    @Test
    public void addProjectTest() {
        Assert.assertNull(projectRepository.findByName("Test Project"));
        final Project project = new Project();
        project.setName("Test Project");
        projectRepository.save(project);
        Assert.assertNotNull(projectRepository.findByName("Test Project"));
    }

}
