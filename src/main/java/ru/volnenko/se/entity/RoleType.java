package ru.volnenko.se.entity;

public enum RoleType {
    ADMIN,
    MANAGER;
}
