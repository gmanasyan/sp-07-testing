<jsp:include page="parts/header.jsp"/>

<div class="box">

<h2>Add Project</h2>

    <form method="post" action="add-project">
        <dl>
            <dt>Name of project:</dt>
            <dd><input type="text" value="" size=40 name="name" required></dd>
        </dl>
        <dl>
            <dt>Start date:</dt>
            <dd><input type="date" name="dateBegin" required></dd>
        </dl>
        <dl>
            <dt>End date:</dt>
            <dd><input type="date" name="dateEnd" required></dd>
        </dl>
        <button type="submit">Save</button>
        <button onclick="window.history.back()" type="button">Cancel</button>
    </form>

</div>

<jsp:include page="parts/footer.jsp"/>