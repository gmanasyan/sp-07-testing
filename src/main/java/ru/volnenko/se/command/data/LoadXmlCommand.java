package ru.volnenko.se.command.data;

import org.springframework.stereotype.Component;
import ru.volnenko.se.command.AbstractCommand;
import ru.volnenko.se.entity.Domain;

@Component
public class LoadXmlCommand extends AbstractCommand {

    @Override
    public String command() {
        return "data-load-xml";
    }

    @Override
    public String description() {
        return "Load Xml file";
    }

    @Override
    public void execute() throws Exception {
        Domain domain = dataService.loadXml();
        if (domain == null) {
            System.out.println("Пустой файл.");
            return;
        }
        projectService.merge(domain.getProjects());
        taskService.merge(domain.getTasks());
        System.out.println("[LOADED FROM XML]");

    }
}
