package ru.volnenko.se.api.service;

import ru.volnenko.se.entity.Domain;

public interface IDataService {

    String exportJson(Domain data);

    void saveJson(Domain data);

    Domain loadJson();

    void saveXml(Domain data);

    Domain loadXml();

}
