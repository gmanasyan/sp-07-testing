package ru.volnenko.se.command.data;

import org.springframework.stereotype.Component;
import ru.volnenko.se.command.AbstractCommand;
import ru.volnenko.se.entity.Domain;

@Component
public class LoadJsonCommand extends AbstractCommand {

    @Override
    public String command() {
        return "data-load-json";
    }

    @Override
    public String description() {
        return "Load Json file";
    }

    @Override
    public void execute() throws Exception {
        Domain domain = dataService.loadJson();
        if (domain == null) {
            System.out.println("Пустой файл.");
            return;
        }
        projectService.merge(domain.getProjects());
        taskService.merge(domain.getTasks());
        System.out.println("[LOADED FROM JSON]");
    }
}
