package ru.volnenko.se.service;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import ru.volnenko.se.entity.Project;
import ru.volnenko.se.entity.Task;
import ru.volnenko.se.repository.ProjectRepository;
import ru.volnenko.se.repository.TaskRepository;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.AdditionalAnswers.returnsFirstArg;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

/**
 *  Classic unit test without spring container
 *  - using mockito
 *  - using assertJ
 *  - creating manually taskService instance by constructor
 *  - mocking DB operations
 */
public class TaskServiceTest {

    private TaskService taskService;

    @Mock
    private TaskRepository taskRepository;

    @Mock
    private ProjectRepository projectRepository;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        taskService = new TaskService(taskRepository, projectRepository);
    }

    @Test
    public void createTaskWithProjectTest() {

        Project project = new Project();
        project.setName("ProjectName");

        when(projectRepository.findByName(any(String.class))).thenReturn(project);
        when(taskRepository.save(any(Task.class))).then(returnsFirstArg());

        Task resultTask = taskService.createTaskByProject("ProjectName", "TaskName");

        assertThat(resultTask.getName()).isEqualTo("TaskName");
        assertThat(resultTask.getProject().getName()).isEqualTo("ProjectName");
    }

}
