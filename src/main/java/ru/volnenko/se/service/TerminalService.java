package ru.volnenko.se.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.volnenko.se.api.service.ITerminalService;
import ru.volnenko.se.command.AbstractCommand;
import ru.volnenko.se.error.CommandCorruptException;

import java.util.*;

/**
 * @author Denis Volnenko
 */
@Service
public final class TerminalService implements ITerminalService {

    private final Scanner scanner = new Scanner(System.in);

    @Autowired
    private List<AbstractCommand> commandsList;

    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    @Override
    public void register() {
        for (AbstractCommand command : commandsList) {
            final String cliCommand = command.command();
            final String cliDescription = command.description();
            if (cliCommand == null || cliCommand.isEmpty()) throw new CommandCorruptException();
            if (cliDescription == null || cliDescription.isEmpty()) throw new CommandCorruptException();
            commands.put(cliCommand, command);
        }
    }

    @Override
    public String nextLine() {
        return scanner.nextLine();
    }

    @Override
    public Integer nextInteger() {
        final String value = nextLine();
        if (value == null || value.isEmpty()) return null;
        try {
            return Integer.parseInt(value);
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public List<AbstractCommand> getCommands() {
        return new ArrayList<>(commands.values());
    }

    @Override
    public AbstractCommand getCommand(String command) {
        return commands.get(command);
    }
}
