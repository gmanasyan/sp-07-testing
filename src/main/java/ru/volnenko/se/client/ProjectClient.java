package ru.volnenko.se.client;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import ru.volnenko.se.entity.Project;
import ru.volnenko.se.entity.Task;

import java.util.List;

public class ProjectClient {

    public static void main(String[] args) {
        getProjects();
        getProject();
    }

    public static void getProjects() {
        String url = "http://localhost:8080/taskmanager/api/projects/";
        RestTemplate template = new RestTemplate();
        ResponseEntity<Project[]> responseEntity = template.getForEntity(
                url, Project[].class);
        Project[] objects = responseEntity.getBody();
        System.out.println(objects);
    }

    public static void getProject() {
        String url = "http://localhost:8080/taskmanager/api/tasks/68d41eb1-08e6-4fe3-ae69-116f47c1a86b";
        RestTemplate template = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        System.out.println(template.getForObject(url, Task.class));
    }

}
